/*
 * gauss.cpp
 *
 *  Created on: 2 ��� 2018 �.
 *      Author: Chibik
 */

#include "gauss.h"
#include <vector>
#include <iostream>

 gauss::gauss() {
	// TODO Auto-generated constructor stub

}

 gauss::~gauss() {
	// TODO Auto-generated destructor stub
}

double gauss::legendre (double a, double b, int n, double (*func)(double))
{
	long double sum=0;
for (int i = 0; i<=n-1; i++){
	sum += Legendre[n-2][1][i]*(*func)(Legendre[n-2][0][i]*(b-a)/2+(b+a)/2);
}
sum*=(b-a)/2;
return sum;
}

double 	gauss::laguerre(int n, double (*func)(double)){
	long double sum=0;
	for (int i = 0; i<=n-1; i++){
		sum += Laguerre[n-2][1][i]*(*func)(Laguerre[n-2][0][i]);
	}
	return sum;
};
/*
gauss::trapezoidal_rule(double a, double b, int n, double (*func)(double))
{
double trapez_sum;
double fa, fb, x, step;
int j;
step=(b-a)/((double) n);
fa=(*func)(a)/2. ;
fb=(*func)(b)/2. ;
TrapezSum=0.;
for (j=1; j <= n-1; j++){
x=j*step+a;
trapez_sum+=(*func)(x);
}
trapez_sum=(trapez_um+fb+fa)*step;
return trapez_sum;
}

gauss::rectangle_rule(double a, double b, int n, double (*func)(double))
{
double rectangle_sum;
double fa, fb, x, step;
int j;
step=(b-a)/((double) n);
rectangle_sum=0.;
for (j = 0; j <= n; j++){
x = (j+0.5)*step+; // midpoint of a given rectangle
rectangle_sum+=(*func)(x); // add value of function.
}
rectangle_sum *= step; // multiply with step length.
return rectangle_sum;
}
*/
