#include "gauss.h"
#include <math.h>
#include <iostream>
#include <vector>
using namespace std;
gauss Gauss;

double Init(double x){
	return x/(1-exp(-2*x));
}
double (*pInit)(double) = &Init;
int main()
{
double a, b;
int n;

cin >> a >> b >> n;

cout << Gauss.laguerre(n, pInit)<< '\n';

return 0;
}


